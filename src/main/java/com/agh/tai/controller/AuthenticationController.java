package com.agh.tai.controller;

import com.agh.tai.model.User;
import com.agh.tai.service.authentication.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.security.SocialAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class AuthenticationController {
    private static Logger log = Logger.getLogger(AuthenticationController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/", method = GET)
    public String getDefaultView() {
        return "index";
    }

    @RequestMapping(value = "/signup", method = GET)
    public ModelAndView getSignupForm(ModelAndView model, WebRequest request, HttpSession session) {
        Connection<?> connection = ProviderSignInUtils.getConnection(request);
        UserProfile userProfile = connection.fetchUserProfile();

        if (!userService.userAccountExistsForConnection(connection)) {
            log.info("User " + userProfile.getUsername() + " hasn't got account. Redirect to signup page");
            session.setAttribute("userProfile", userProfile);
            session.setAttribute("connection", connection);
            model.addObject("firstName", userProfile.getFirstName());
            model.addObject("lastName", userProfile.getLastName());
            model.addObject("username", userProfile.getUsername());
            model.addObject("email", userProfile.getEmail());
            model.setViewName("signup");
            return model;
        }

        if (!userService.userHasSession()) {
            log.info("User " + userProfile.getUsername() + " hasn't got active session. Creating session");
            User user = userService.getUserForUsername(userProfile.getUsername());
            userService.createUserSession(user, connection);
        }

        // User has accout and is logged in (has session)
        model.setViewName("redirect:/event");
        return model;
    }

    @RequestMapping(value = "/signup", method = POST)
    public ModelAndView createAccount(
            ModelAndView model,
            HttpSession session,
            WebRequest request,
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String username,
            @RequestParam String email,
            @RequestParam(required = false) Boolean isCreator) {

        Connection<?> connection = (Connection<?>) session.getAttribute("connection");

        List<String> errors = new ArrayList<>();
        model.addObject("errors", errors);
        validateErrors(firstName, lastName, username, email, errors);

        if (CollectionUtils.isEmpty(errors)) {
            User user = userService.createUserAccount(request, firstName, lastName, username, email, isCreator);
            log.info("Created account for user: " + user.getUsername());

            SecurityContextHolder.getContext().setAuthentication(new SocialAuthenticationToken(connection, new HashMap<String, String>()));
            userService.createUserSession(user, connection);
            log.info("Created session for user: " + user.getUsername());

            model.setViewName("redirect:/event");
            return model;
        }
        log.warn("User didn't fill all form to signup");
        model.setViewName("/signup");
        return model;
    }

    private void validateErrors(String firstName, String lastName, String username, String email, List<String> errors) {
        if (isEmpty(firstName)) {
            errors.add("empty_firstName");
        }
        if (isEmpty(lastName)) {
            errors.add("empty_lastName");
        }
        if (isEmpty(username)) {
            errors.add("empty_username");
        }
        if (isEmpty(email)) {
            errors.add("empty_email");
        }
    }
}

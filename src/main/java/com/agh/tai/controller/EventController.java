package com.agh.tai.controller;

import com.agh.tai.model.Event;
import com.agh.tai.model.Role;
import com.agh.tai.model.TweetEntity;
import com.agh.tai.model.User;
import com.agh.tai.persistence.EventDAO;
import com.agh.tai.persistence.TweetDAO;
import com.agh.tai.service.NotificationService;
import com.agh.tai.service.TwitterRESTService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class EventController {
    private static Logger log = Logger.getLogger(EventController.class);

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private TwitterRESTService twitterRESTService;
    @Autowired
    private EventDAO eventDAO;
    @Autowired
    private TweetDAO tweetDAO;

    @RequestMapping(value = "/event", method = GET)
    public ModelAndView getAllEvents(ModelAndView model) {
        List<Event> events = eventDAO.getAll();
        model.setViewName("events");
        model.addObject("events", events);
        return model;
    }

    @RequestMapping(value = "/event/{eventId}", method = GET)
    public ModelAndView getEvent(ModelAndView model, @PathVariable Long eventId) {
        Event event = eventDAO.getForId(eventId);
        if (event == null) {
            log.warn("Event with id " + eventId + " not found");
            model.setViewName("redirect:/event?nosuchevent");
            return model;
        }

        List<TweetEntity> tweetsForEvent = tweetDAO.getForEvent(event);
        model.addObject("tweets", tweetsForEvent);
        model.addObject("event", event);
        model.setViewName("event");
        return model;
    }

    @RequestMapping(value = "/event/add", method = POST)
    public ModelAndView addNewEvent(
            ModelAndView model,
            @RequestParam String name,
            @RequestParam String description,
            @RequestParam String hashtag) {

        List<String> errors = new ArrayList<>();
        model.addObject("errors", errors);
        model.setViewName("events");

        if (getActiveUser().getRole() != Role.ROLE_CREATOR) {
            log.warn("User " + getActiveUser().getUsername() + " can't create event");
            errors.add("not_authorized");
        }
        if (eventDAO.getForHashtag(hashtag) != null) {
            log.warn("User " + getActiveUser().getUsername() + " attempted to create already existing event");
            errors.add("event_hashtag_exists");
        }
        if (CollectionUtils.isNotEmpty(errors)) {
            return model;
        }

        Event event = new Event(name, description, hashtag);
        eventDAO.save(event);
        notificationService.onEventAdded(event);

        model.setViewName("redirect:/event");
        return model;
    }


    @RequestMapping(value = "/event/{eventId}/tweet", method = POST)
    public String tweetOnEvent(
            @PathVariable Long eventId,
            @RequestParam String tweetText) {

            log.info("User " + getActiveUser().getUsername() + " posted tweet on event with id: " + eventId);
            twitterRESTService.tweetOnEvent(eventId, tweetText);
            return "redirect:/event/" + eventId;
    }

    private User getActiveUser() {
        return twitterRESTService.getUser();
    }
}

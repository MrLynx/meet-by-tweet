package com.agh.tai.model;

import java.util.HashSet;
import java.util.Set;

public class Event {
    private Long id;
    private String name;
    private String description;
    private String hashtag;
    private Set<TweetEntity> tweets = new HashSet<>();

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public Event() {
    }

    public Event(String name, String description, String hashtag) {
        this.name = name;
        this.description = description;
        this.hashtag = hashtag;
    }

    public Event(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Set<TweetEntity> getTweets() {
        return tweets;
    }

    public void setTweets(Set<TweetEntity> tweets) {
        this.tweets = tweets;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

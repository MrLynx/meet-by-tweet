package com.agh.tai.model;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    ROLE_COMMENTER(0),
    ROLE_CREATOR(1);

    private int bit;

    Role(int bit) {
        this.bit = bit;
    }

    @Override
    public String getAuthority() {
        return toString();
    }

}

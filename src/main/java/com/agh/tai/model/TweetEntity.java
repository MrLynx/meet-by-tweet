package com.agh.tai.model;

public class TweetEntity {
    private Long id;
    private Long twitterId;
    private Event event;
    private String username;
    private String text;

    public TweetEntity() {
    }

    public TweetEntity(Long twitterId, String text, String username) {
        this.twitterId = twitterId;
        this.text = text;
        this.username = username;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getTwitterId() {
        return twitterId;
    }

    public void setTwitterId(Long twitterId) {
        this.twitterId = twitterId;
    }
}

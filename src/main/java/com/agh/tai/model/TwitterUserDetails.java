package com.agh.tai.model;

import org.springframework.social.security.SocialUser;

import static com.google.common.collect.Lists.newArrayList;

public class TwitterUserDetails extends SocialUser {
    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private Role role;

    public TwitterUserDetails(String firstName, String lastName, String username, String email, Role role) {
        super(username, "", role == Role.ROLE_CREATOR ? newArrayList(Role.ROLE_CREATOR, Role.ROLE_COMMENTER) : newArrayList(Role.ROLE_COMMENTER));
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.role = role;
    }

}

package com.agh.tai.persistence;

import com.agh.tai.model.Event;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class EventDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public List<Event> getAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("FROM Event").list();
    }

    public void save(Event event) {
        Session session = sessionFactory.getCurrentSession();
        session.save(event);
    }

    public Event getForId(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Event event = (Event) session.get(Event.class, id);
        return event;
    }

    public Event getForHashtag(String hashtag) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Event.class);
        criteria.add(Restrictions.eq("hashtag", hashtag));
        return (Event) criteria.uniqueResult();
    }
}
package com.agh.tai.persistence;

import com.agh.tai.model.Event;
import com.agh.tai.model.TweetEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class TweetDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public void save(TweetEntity tweet) {
        Session session = sessionFactory.getCurrentSession();
        session.save(tweet);
    }

    public void saveOrUpdate(TweetEntity tweet) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(tweet);
    }

    public TweetEntity getForTwitterId(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(TweetEntity.class);
        criteria.add(Restrictions.eq("twitterId", id));
        return (TweetEntity) criteria.uniqueResult();
    }

    public TweetEntity getForId(Long id) {
        Session session = sessionFactory.getCurrentSession();
        TweetEntity tweet = (TweetEntity) session.get(TweetEntity.class, id);
        return tweet;
    }

    public List<TweetEntity> getAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("FROM TweetEntity").list();
    }

    public List<TweetEntity> getForEvent(Event event) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(TweetEntity.class);
        criteria.add(Restrictions.eq("event", event));
        return criteria.list();
    }
}

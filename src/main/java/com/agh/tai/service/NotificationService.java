package com.agh.tai.service;

import com.agh.tai.model.Event;
import com.agh.tai.model.TweetEntity;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {
    private static Logger log = Logger.getLogger(NotificationService.class);

    @Autowired
    SimpMessagingTemplate messagingTemplate;

    public void onTweetAdded(TweetEntity localTweet) {
        log.info("Notifying websockets about new tweet");
        messagingTemplate.convertAndSend("/topic/tweet", localTweet);
    }

    public void onEventAdded(Event event) {
        log.info("Notifying websockets about new event");
        messagingTemplate.convertAndSend("/topic/event", event);
    }
}

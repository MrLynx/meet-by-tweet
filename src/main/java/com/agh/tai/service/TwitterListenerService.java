package com.agh.tai.service;

import com.agh.tai.model.Event;
import com.agh.tai.model.TweetEntity;
import com.agh.tai.persistence.EventDAO;
import com.agh.tai.persistence.TweetDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.social.twitter.api.HashTagEntity;
import org.springframework.social.twitter.api.SearchResults;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;

import java.util.List;

public class TwitterListenerService {
    private static Logger log = Logger.getLogger(TwitterListenerService.class);

    public static final int FETCH_INTERVAL = 2 * 60 * 1000;
    public static final String QUERY_STRING = "#MeetByTweet";

    @Value("${twitter.consumerKey}")
    private String consumerKey;

    @Value("${twitter.consumerSecret}")
    private String consumerSecret;

    @Value("${twitter.accessToken}")
    private String accessToken;

    @Value("${twitter.accessTokenSecret}")
    private String accessTokenSecret;

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private TweetDAO tweetDAO;
    @Autowired
    private EventDAO eventDAO;

    private Twitter twitter;
    private Long lastTweetId;

    public void init() {
        twitter = new TwitterTemplate(consumerKey, consumerSecret);
    }

    @Scheduled(fixedDelay = FETCH_INTERVAL)
    public void fetchNewTweets() {
        SearchResults searchResults = twitter.searchOperations().search(QUERY_STRING);
        int fetchedTweets = 0;
        for (Tweet tweet : searchResults.getTweets()) {
            TweetEntity tweetEntity = tweetDAO.getForTwitterId(tweet.getId());
            List<HashTagEntity> hashTags = tweet.getEntities().getHashTags();

            if (tweetEntity == null && hashTags.size() > 1) {
                String eventHashtag = hashTags.get(1).getText();
                Event event = eventDAO.getForHashtag(eventHashtag);
                if(event == null) {
                    continue;
                }

                TweetEntity localTweet = createLocalTweetFromRemote(tweet);
                localTweet.setEvent(event);
                tweetDAO.save(localTweet);

                notificationService.onTweetAdded(localTweet);
                fetchedTweets++;
            }
//            else if(hashTags.size() > 1) {
//                String eventHashtag = hashTags.get(1).getText();
//                Event event = eventDAO.getForHashtag(eventHashtag);
//                notificationService.onEventAdded(event);
//            }
        }
        log.info("Scheduled update fetched " + fetchedTweets + " new tweets");
    }

    public TweetEntity createLocalTweetFromRemote(Tweet tweet) {
        return new TweetEntity(
                tweet.getId(),
                tweet.getText(),
                tweet.getFromUser());
    }

    public synchronized void setLastTweetId(Long lastTweetId) {
        this.lastTweetId = lastTweetId;
    }
}

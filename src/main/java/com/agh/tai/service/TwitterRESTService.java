package com.agh.tai.service;

import com.agh.tai.model.Event;
import com.agh.tai.model.TweetEntity;
import com.agh.tai.model.User;
import com.agh.tai.persistence.EventDAO;
import com.agh.tai.persistence.TweetDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.social.connect.Connection;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Service;

@Service
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class TwitterRESTService {
    private static Logger log = Logger.getLogger(TwitterRESTService.class);

    @Autowired
    private TwitterListenerService twitterListenerService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private EventDAO eventDAO;
    @Autowired
    private TweetDAO tweetDAO;

    private boolean hasSession = false;
    private Twitter twitter;
    private User user;

    public void tweetOnEvent(Long eventId, String tweetText) {
        Event event = eventDAO.getForId(eventId);

        String fullText = "#MeetByTweet #" + event.getHashtag() + " " + tweetText;
        Tweet tweetFromServer = twitter.timelineOperations().updateStatus(fullText);
        if (tweetFromServer == null) {
            log.warn("Could not send tweet from user " + user.getUsername() + " to server");
            return;
        }

        TweetEntity tweet = createLocalTweetFromRemote(tweetFromServer);
        tweet.setEvent(event);
        tweetDAO.save(tweet);

        twitterListenerService.setLastTweetId(tweet.getId());
        notificationService.onTweetAdded(tweet);
    }

    public void beginSession(User user, Connection<Twitter> connection) {
        this.user = user;
        this.twitter = connection.getApi();
        this.hasSession = true;
    }

    public boolean hasSession() {
        return hasSession;
    }

    public User getUser() {
        return user;
    }

    public TweetEntity createLocalTweetFromRemote(Tweet tweet) {
        return new TweetEntity(
                tweet.getId(),
                tweet.getText(),
                tweet.getFromUser());
    }
}

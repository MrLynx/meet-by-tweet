package com.agh.tai.service.authentication;

import com.agh.tai.model.User;
import com.agh.tai.persistence.UserDAO;
import com.agh.tai.service.TwitterRESTService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.social.security.SocialAuthenticationToken;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Collections;

@Service
public class SpringSecuritySignInAdapter implements SignInAdapter {
    private static Logger log = Logger.getLogger(SpringSecuritySignInAdapter.class);

    @Autowired
    private TwitterRESTService twitterRESTService;
    @Autowired
    private UserDAO userDAO;

    @Override
    public String signIn(String username, Connection<?> connection, NativeWebRequest nativeWebRequest) {
        if (!userHasSession()) {
            log.info("User " + username + " hasn't got active session. Creating session");
            User user = userDAO.getForUsername(username);
            createUserSession(user, connection);
        }
        SecurityContextHolder.getContext().setAuthentication(new SocialAuthenticationToken(connection, Collections.<String, String>emptyMap()));
        return "/event";
    }

    private void createUserSession(User user, Connection<?> connection) {
        twitterRESTService.beginSession(user, (Connection<Twitter>) connection);
    }

    private boolean userHasSession() {
        return twitterRESTService.hasSession();
    }
}

package com.agh.tai.service.authentication;

import com.agh.tai.model.TwitterUserDetails;
import com.agh.tai.model.User;
import com.agh.tai.persistence.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class TwitterUserDetailsService implements SocialUserDetailsService {
    @Autowired
    private UserDAO userDAO;

    @Override
    public SocialUserDetails loadUserByUserId(String username) throws UsernameNotFoundException, DataAccessException {
        User user = userDAO.getForUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("No user for username: " + username);
        }

        return new TwitterUserDetails(
                user.getFirstName(),
                user.getLastName(),
                user.getUsername(),
                user.getEmail(),
                user.getRole());
    }
}

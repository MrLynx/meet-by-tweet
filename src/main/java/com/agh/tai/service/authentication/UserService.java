package com.agh.tai.service.authentication;

import com.agh.tai.model.Role;
import com.agh.tai.model.User;
import com.agh.tai.persistence.UserDAO;
import com.agh.tai.service.TwitterRESTService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.WebRequest;

@Service
public class UserService {
    @Autowired
    private UsersConnectionRepository usersConnectionRepository;
    @Autowired
    private TwitterRESTService twitterRESTService;
    @Autowired
    private UserDAO userDAO;

    public User createUserAccount(WebRequest request, String firstName, String lastName, String username, String email, Boolean isCreator) {
        User user = new User(firstName, lastName, username, email, isCreator != null ? Role.ROLE_CREATOR : Role.ROLE_COMMENTER);
        usersConnectionRepository.createConnectionRepository(username); // no more redirects to signup
        userDAO.save(user);
        ProviderSignInUtils.handlePostSignUp(username, request);
        return user;
    }

    public void createUserSession(User user, Connection<?> connection) {
        twitterRESTService.beginSession(user, (Connection<Twitter>) connection);
    }

    public boolean userAccountExistsForConnection(Connection<?> connection) {
        return CollectionUtils.isNotEmpty(usersConnectionRepository.findUserIdsWithConnection(connection));
    }

    public User getUserForUsername(String username) {
        return userDAO.getForUsername(username);
    }

    public boolean userHasSession() {
        return twitterRESTService.hasSession();
    }
}

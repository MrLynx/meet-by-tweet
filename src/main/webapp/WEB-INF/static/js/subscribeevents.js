$(function() {
    console.log("starting socket");
    var sock = new SockJS('http://meetbytweet-mrlynx.rhcloud.com/endpoint/');
    var stompClient = Stomp.over(sock);
    stompClient.connect({}, function(frame) {
        console.log('received frame');
        console.log(frame);
        console.log('received frame EEE');

        stompClient.subscribe("/topic/event", function(msg) {
            console.log('received message');
            console.log(msg)
            console.log('received message EEE');

            var event = JSON.parse(msg.body);
            var words = event.description.split(' ');

            console.log('event:');
            console.log(event);

            var panelHeading = $('<div>', {class: "panel-heading"});
            panelHeading.append('<h3>' + event.name + '</h3>');
            panelHeading.append('<small>#' + event.hashtag + '</small>');

            var panelBody = $('<div>', {class: "panel-body"});
            if(words.length < 11) {
                panelBody.append('<p>' + event.description + '</p>');
            } else {
                panelBody.append('<p>' + words.slice(0, 10).join(' ') + '...</p>');
            }
            panelBody.append('<a href="' + '/event/' + event.id + '">See more</a>');

            var panel = $('<div>', {class: "panel panel-default"});
            panel.append(panelHeading);
            panel.append(panelBody);

            $('#eventsDiv').append(panel);
        });
    });
});
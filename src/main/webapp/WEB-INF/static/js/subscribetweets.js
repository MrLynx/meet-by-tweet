$(function() {
    console.log("starting socket");
    var sock = new SockJS('http://meetbytweet-mrlynx.rhcloud.com/endpoint/');
    var stompClient = Stomp.over(sock);
    stompClient.connect({}, function(frame) {
        console.log('received frame');
        console.log(frame);
        console.log('received frame EEE');

        stompClient.subscribe("/topic/tweet", function(msg) {
            console.log('received message');
            console.log(msg)
            console.log('received message EEE');

            var tweet = JSON.parse(msg.body);
            console.log('tweet:');
            console.log(tweet);

            var blockquote = $('<blockquote></blockquote>');
            blockquote.append('<p>' + tweet.text + '</p>');
            blockquote.append('<small>@' + tweet.username + '</small>');
            $('#commentsDiv').append(blockquote);
        });
    });
});